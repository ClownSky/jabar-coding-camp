//Luas Persegi Panjang
console.log("Soal No 1");
function luas() {
  let lebar = 10;
  let panjang = 25;
  let luas = panjang * lebar;

  return luas;
}
console.log(luas());

//Keliling Persegi Panjang
function keliling() {
  let lebar = 10;
  let panjang = 25;
  let keliling = 2 * (panjang + lebar);

  return keliling;
}
console.log(keliling());

//Soal No 2
console.log("\n" + "Soal No 2");

const newFunction = (firstName, lastName) => {
  const namaLengkap = firstName + " " + lastName;
  return console.log(namaLengkap);
};

newFunction("William", "Imoh");

// Soal No 3
console.log("\n" + "Soal No 3");

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "Playing football",
};

const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);

//Soal No 4
console.log("\n" + "Soal No 4");
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [west, east];
console.log(combined);

//Soal No 5
console.log("\n" + "Soal No 5");
const planet = "Earth";
const view = "Glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;
console.log(before);

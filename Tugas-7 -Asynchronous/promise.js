// di file promise.js
function readBooksPromise(time, book) {
  console.log(`Saya Mulai Membaca ${book.name}`);
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      let sisaWaktu = time - book.timeSpent;
      if (sisaWaktu >= 0) {
        console.log(`Saya Sudah Selesai Membaca ${book.name}, Sisa Waktu Saya ${sisaWaktu}`);
        resolve(sisaWaktu);
      } else {
        console.log(`Saya Sudah Tidak Punya Waktu Untuk Baca ${book.name}`);
        reject(sisaWaktu);
      }
    }, book.timeSpent);
  });
}

module.exports = readBooksPromise;

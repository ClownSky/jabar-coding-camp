var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "Komik", timeSpent: 1000 },
];
const lookupBook = (remainingWaktu, books, index) => {
  readBooks(remainingWaktu, books[index], function (waktu) {
    const bukuSelanjutnya = index + 1;
    if (bukuSelanjutnya < books.length) {
      lookupBook(waktu, books, bukuSelanjutnya);
    }
  });
};
// Soal No 1
console.log("Soal No 1");
lookupBook(10000, books, 0);

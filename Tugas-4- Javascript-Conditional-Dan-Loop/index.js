//Soal No 1
console.log("Soal No 1");
var nilai = 22;
if (nilai > 85) {
  console.log("Nilai A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("Nilai B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("Nilai C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("Nilai D");
} else {
  console.log("Nilai E \n");
}

//Soal No 2
console.log("Soal No 2");
var tanggal = 30;
var bulan = 10;
var tahun = 2001;

switch (bulan) {
  case 1: {
    console.log(tanggal + " Januari " + tahun);
    break;
  }
  case 2: {
    console.log(tanggal + " Februari " + tahun);
    break;
  }
  case 3: {
    console.log(tanggal + " Maret " + tahun);
    break;
  }
  case 4: {
    console.log(tanggal + " April " + tahun);
    break;
  }
  case 5: {
    console.log(tanggal + " Mei " + tahun);
    break;
  }
  case 6: {
    console.log(tanggal + " Juni " + tahun);
    break;
  }
  case 7: {
    console.log(tanggal + " Juli " + tahun);
    break;
  }
  case 8: {
    console.log(tanggal + " Agustus " + tahun);
    break;
  }
  case 9: {
    console.log(tanggal + " September " + tahun);
    break;
  }
  case 10: {
    console.log(tanggal + " Oktober " + tahun);
    break;
  }
  case 11: {
    console.log(tanggal + " November " + tahun);
    break;
  }
  case 12: {
    console.log(tanggal + " Desember " + tahun);
    break;
  }
  default: {
    console.log("Bukan Bulan");
  }
}

//Soal No 3
console.log("\n" + "Soal No 3");
var n = 1;
console.log("n = " + n);
while (n <= 3) {
  console.log("#".repeat(n));
  n++;
}

var n = 1;
console.log("n = 7");
while (n <= 7) {
  console.log("#".repeat(n));
  n++;
}

//Soal No 4

console.log("\n" + "Soal No 4");
console.log("Keluaran untuk m = 3");
var cinta;
for (let m = 1; m <= 3; m++) {
  if (m == 1) {
    cinta = "Programming";
  } else if (m == 2) {
    cinta = "Javascript";
  } else {
    cinta = "VueJS";
  }
  console.log(m + " - I Love " + cinta);
  if (m == 3) {
    console.log("=".repeat(m));
  }
}

console.log("\n" + "Keluaran untuk m = 7");
var cinta;
var counter = 1;
for (let m = 1; m <= 7; m++) {
  if (counter == 1) {
    cinta = "Programming";
  } else if (counter == 2) {
    cinta = "Javascript";
  } else {
    counter = 1;
    cinta = "VueJS";
  }
  console.log(m + " - I Love " + cinta);
  if (m % 3 == 0) {
    counter == 0;
    console.log("=".repeat(m));
  } else {
    counter++;
  }
}

console.log("\n" + "Keluaran untuk m = 10");
var cinta;
var counter = 1;
for (let m = 1; m <= 10; m++) {
  if (counter == 1) {
    cinta = "Programming";
  } else if (counter == 2) {
    cinta = "Javascript";
  } else {
    counter = 1;
    cinta = "VueJS";
  }
  console.log(m + " - I Love " + cinta);
  if (m % 3 == 0) {
    counter == 0;
    console.log("=".repeat(m));
  } else {
    counter++;
  }
}

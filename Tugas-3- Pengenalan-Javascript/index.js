//Soal No 1
var pertama = "Saya Sangat Senang Hari Ini";
var kedua = "Belajar Javascript Itu Keren";
var kataPertama = pertama.substring(0, 5) + pertama.substring(12, 19);
var kataKedua = kedua.substring(0, 8) + kedua.substring(8, 18);
var jawabanSoal = kataPertama.concat(kataKedua);
var hasil = jawabanSoal;
console.log("Jawaban Soal No 1 " + hasil);

//Soal No 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var angkaPertama = parseInt(kataPertama);
var angkaKedua = parseInt(kataKedua);
var angkaKetiga = parseInt(kataKetiga);
var angkaKeempat = parseInt(kataKeempat);
var hitung = angkaPertama + angkaKedua * angkaKetiga + angkaKeempat;
console.log("Jawaban Soal No 2 " + hitung);

//Soal No 3
var kalimat = "Wah Javascript Itu Keren Sekali";
var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);
console.log("Jawaban Soal No 3  ");
console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);

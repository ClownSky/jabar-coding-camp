// Soal No 1
console.log("Soal No 1");
var Hewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var sortDaftarHewan = Hewan.sort();
for (i = 0; i <= sortDaftarHewan.length - 1; i++) {
  console.log(sortDaftarHewan[i]);
}
// Soal No 2
console.log("\n" + "Soal No 2");
var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" };
function introduce(data) {
  var tampilkanIntroduce = "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby;
  return tampilkanIntroduce;
}

var perkenalan = introduce(data);
console.log(perkenalan);

// Soal No 3
console.log("\n" + "Soal No 3");
const hurufVokal = ["a", "i", "u", "e", "o"];

function hitung_huruf_vokal(data) {
  var dataLower = data.toLowerCase();
  var dataSplit = dataLower.split("");
  var counter = 0;
  for (i = 0; i <= dataSplit.length - 1; i++) {
    if (hurufVokal.includes(dataSplit[i])) {
      counter++;
    }
  }
  return counter;
}
var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2);

// Soal No 4
console.log("\n" + "Soal No 4");

function hitung(input) {
  var output = [-2, 0, 2, 4, 6, 8];
  return output[input];
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));
